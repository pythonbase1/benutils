""" benutils/version.py """

__version__ = "0.5.3"

# 0.5.3   (28/02/2024): Enhance PidCore, PidVelocity with new capabilities.
# 0.5.1   (01/09/2023): Update/Review sdr classes.
# 0.5.0   (07/04/2021): Use snake case over camel case for name of signals in
#                       sdr entities.
# 0.2.0   (13/12/2019): Add support of signal/slot facilities in pure python
#                       using signalslot package.
# 0.1.5   (31/10/2019): Correct error in sdr/* files
# 0.1.4   (19/07/2019): Add usefull method to misc/datacontainer.py
# 0.1.3   (19/03/2019): Moves to PyQt5
# 0.1.2   (13/03/2019): Add initial condition to 'sdr files'
# 0.1.1   (27/02/2018): Add classes of handler for data logging mechanism
# 0.1.0a0 (20/02/2018): Initial version
